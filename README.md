# Project Title

## Table of Contents

- [About](#about)

## About <a name = "about"></a>

Pré-requisitos
Xcode
É um ambiente de desenvolvimento integrado e software livre da Apple Inc. Também será necessário autorizar o uso do iOS Simulator:

sudo authorize_ios
Android studio
É a IDE oficial para criação de aplicativos em todos os tipos de dispositivos android. Link para download do android studio: https://developer.android.com/studio/index.html?hl=pt-br

Java
Link para download: http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html

Após instalar é necessário setar as variáveis de ambiente JAVA_HOME e ANDROID_HOME no arquivo bash_profile.

Ex: Abrir o terminal e digitar:

open ~/.bash_profile   # abre o arquivo bash_profile e cole os arquivos
export ANDROID_HOME=/Users/[SEU USUARIO]/Library/Android/sdk export PATH=$PATH:$ANDROID_HOME/bin:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$ANDROID_HOME/lib:$ANDROID_HOME/tools/lib:$ANDROID_HOME/bin
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_111.jdk/Contents/Home export PATH=$PATH:$JAVA_HOME export PATH=$PATH:$JAVA_HOME/bin

Homebrew
O Homebrew instala os pacotes que não vem por padrão no sistema da Apple. Para instalar o homebrew cole no seu terminal:

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
Node JS
O Appium é um servidor HTTP escrito em node.js que cria e manipula várias sessões do WebDriver para diferentes plataformas, como iOS e Android. A automação de aplicativos móveis híbridos e nativos para Android e iOS é uma função chave administrada pelo Appium, um servidor node.js. A interação entre o servidor node.js e as bibliotecas de client do Selenium é o que, em última análise, funciona em conjunto com a aplicação móvel. Para instalar basta colar no terminal:

brew install npm    #instalar o appium via(source) npn(Node JS Package Manager)
npm --version       #versão atual do npm
brew install node   #instalar o node
node --version      #versão atual do node
Appium-doctor
Verificar se todas as dependências do Appium são atendidas e se todas as dependências estão configuradas corretamente. Para instalar o appium-doctor basta colar no seu terminal:

npm install -g appium-doctor  # instalar o appium-doctor
Uma vez que o node.js, npm e o appium-doctor estão instalados, você pode usar o comando abaixo para verificar se todas as dependências do appium são atendidas, execute o comando abaixo:
appium-doctor             # verificar todas as dependencia necessarias para usar o appium
appium-doctor --android   # verificar as dependencias somente para android
appium-doctor --ios       # verificar as dependencias somente para ios
Instalar o appium
instalar o Appium
Execute o comando abaixo para instalar o appium:

npm install -g appium
Após o término, inicie o servidor do Appium pelo seguinte comando:
appium                    # inicia o servidor do appium
Para atualizar o appium, é necessário executar: npm install -g appium novamente.
Appium Client
Ruby
Ruby https://github.com/appium/ruby_lib

Execute o comando abaixo para instalar o appium client:

npm install wd            # cliente do appium
Desired Capabilities
São um conjunto de chave/valor que são enviados ao Appium Server para informar qual tipo de sessão o qual deseja iniciar. É através do Desired Capabilities que informamos em qual dispositivo queremos executar, e quais as configurações iniciais. Exemplos:

Capabilities para Android
[caps] platformName = "Android" deviceName = 'Nexus_5_API_23_mars' app = '/Users/[SEU USUARIO]/dev/android/app/build/outputs/apk/nome.apk'

Capabilities para iOS
[caps] platformName = "iOS" platformVersion = "11.2" deviceName = "iPhone 6 Plus" app = "/Users/[SEU USUARIO]/dev/appium/build/mock/simulator/nome.app"

Executar o projeto
Com tag
cucumber -p android -t@smoke_test