require 'yaml'

def import_data(ambiente)
  dir = "#{Dir.pwd}/features/support/data/#{ambiente}"
  data = @data
  Dir.foreach(dir) do |filename|
    next if filename == '.' or filename == '..'
    area = File.basename(filename, ".yaml").to_s
    data["#{area}"] = YAML.load_file("#{dir}/#{filename}")
  end
  @data = data
end
