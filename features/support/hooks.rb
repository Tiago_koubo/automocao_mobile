require_relative 'data'
require_relative 'utils'
require 'report_builder'
require 'launchy'

Before do
  $driver.start_driver
end

Before do |scenario|
  $logger = Logger.new(STDOUT)
  @start_all = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  @start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  @scenario_name = scenario.name.gsub(/([_@#!%()\-=;><,{}\~\[\]\.\/\?\"\*\^\$\+\-]+)/, '')
  ##import_data environment
end

After do |scenario|
  if scenario.failed?
     printscreen
  end
  finish_all = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  diff = (finish_all - @start_all).round(2)
  puts "Final: #{diff}"
  $driver.reset
end

AfterStep do |result, step|
  finish = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  diff = (finish - @start).round(2)
  @start = finish
  puts diff
  expect(result).to be_a(Cucumber::Core::Test::Result::Passed)
end

at_exit do
  $driver.driver_quit
   if ENV['REPORT'] == 'json'
     ReportBuilder.input_path = "cucumber.json"

     ReportBuilder.configure do |config|
       config.report_path = ""
       config.report_types = [:json, :html]

     options = {
        report_title: "Automacao QA"
     }
     ReportBuilder.build_report options
     Launchy.open("./test_report.html")
    end
  end
end