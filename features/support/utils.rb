
require 'yaml'
require 'logger'

def keycode event
  case event
  when :enter
    66
  when :search
    84
  end
end

def wait(tempo=30)
    Selenium::WebDriver::Wait.new :timeout => tempo
end

def wait_for(el)
  wait.until {$driver.find_element(":#{el['tipo_busca']}", el['value']).displayed? }
  $logger.info("Aguardou a exibição do elemento #{el['value']} usando o tipo de busca #{el['tipo_busca']}")
end

def click(el)
  wait_for_element_exist(el)
  $driver.find_element(":#{el['tipo_busca']}", el['value']).click
  $logger.info('Clicou no botão ' + el['value'] + ' usando o tipo de busca ' + el['tipo_busca'])
end

def wait_for_element_exist(el)
  wait.until {element_exists? el}
  $logger.info("Aguardou a existência do elemento #{el['value']} usando o tipo de busca #{el['tipo_busca']}")
end

def element_exists?(el)
  $logger.info("Verificando se existe o elemento #{el['value']} usando o tipo de busca #{el['tipo_busca']}")
  return $driver.find_element(":#{el['tipo_busca']}", el['value']).count > 0
end

def send_keys(el, text)
  element = $driver.find_element(":#{el['tipo_busca']}", el['value'])
  element.clear
  element.send_keys text
  $logger.info("Preencheu o campo #{el} usando o tipo de busca #{el['tipo_busca']} com o valor #{text}")
end

def press_keycode keycode
  $driver.press_keycode(keycode)
end

def get_element(screen)
  $platform = ENV['PLATAFORMA'].downcase
  dir = "#{Dir.pwd}/features/elements/#{screen}_screen.yml"
  screen_mappings = YAML.load_file(dir)
  @mappings = screen_mappings[$platform][screen]
end

def printscreen 
  screenshot 'image.png'
  embed("image.png", 'image/png')
end

