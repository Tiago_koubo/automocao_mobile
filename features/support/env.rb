require 'dotenv/load'
require 'pry'
require 'rspec/expectations'
require 'appium_lib'
require 'appium_lib_core'
require_relative 'utils'

if ENV['PLATAFORMA'] == 'ios'
  opts = {
    caps: {
        'platformName' => 'iOS',
        'platformVersion' => ENV['IOS_PLATFORM_VERSION'],
        'deviceName' => ENV['IOS_DEVICE_NAME'],
        'app' => ENV['IOS_IPA_PATH'],
        'automationName' => 'XCUITest'
      },
  }
elsif ENV['PLATAFORMA'] == 'android'
    opts = { 
        caps: {
          'platformName' => 'Android',
          'deviceName' => ENV['ANDROID_DEVICE_NAME'],
          'app' => ENV['APK_PATH'],
          'autoGrantPermissions' => 'true',
          'skipDeviceInitialization' => 'true'
        }
      }
end

opts[:appium_lib] = { 'server_url' => 'http://0.0.0.0:4723/wd/hub' }
opts[:verbose] = ENV['VERBOSE']

puts 'Current Appium configuration set is:'
puts opts

Appium::Driver.new(opts, true)
Appium.promote_appium_methods Object


