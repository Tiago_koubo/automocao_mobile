# encoding: utf-8
#language: pt

@pesquisar_itens
Funcionalidade: TESTE
  Eu teste
  Desejo teste
  Escolhas teste

  @teste @android
  Esquema do Cenario: Teste
    Dado que estou na tela teste
    Quando faço uma busca por teste
    Então verifico que a busca retornou resultados válidos para o teste
