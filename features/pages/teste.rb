# encoding: utf-8

require 'rspec/expectations'

class SearchPage 
  def initialize
    get_element 'teste'
  end

  def teste?
    wait_for_element_exist(@mappings['teste_teste'])
  end
end